# README #

Configuration module for the BostwanaEMR distribution

### What is this repository for? ###

This module provides the following configuration meta-data:

* [ ] Look and feel customizations
* [ ] Patient identifiers

### How do I get set up? ###

* Summary 
1. Download OpenMRS Reference Application 2.12.2 modules from: https://sourceforge.net/projects/openmrs/files/releases/OpenMRS_Reference_Application_2.12.2/referenceapplication-addons-2.12.2.zip/download

2.  Unpack this zip file and copy all modules into the {OPENMRS_HOME}/modules folder

3. Download the OpenMRS Platform 2.5.0 from source

4. Download the following module and upgrade the existing module:
htmlformentry-4.3.0 from https://addons.openmrs.org/show/org.openmrs.module.htmlformentry

5. Download, build and install this module to integrate with the above mentioned packages

6. Run the integrated package as a normal OpenMRS reference Application using https://wiki.openmrs.org/display/docs/Reference+Application+2.11.0 as a sample guide

* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact